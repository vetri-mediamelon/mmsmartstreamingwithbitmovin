//
//  main.m
//  MMSmartStreamingWithBitmovin
//
//  Created by vetri on 09/20/2020.
//  Copyright (c) 2020 vetri. All rights reserved.
//

@import UIKit;
#import "MMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MMAppDelegate class]));
    }
}
