//
//  MMViewController.m
//  MMSmartStreamingWithBitmovin
//
//  Created by vetri on 09/20/2020.
//  Copyright (c) 2020 vetri. All rights reserved.
//

#import "MMViewController.h"
#import <MMSmartStreamingWithBitmovin/BitmovinPlayerIntegrationWrapper.h>
#import <BitmovinPlayer/BitmovinPlayer.h>

@interface MMViewController ()

@end

@implementation MMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
