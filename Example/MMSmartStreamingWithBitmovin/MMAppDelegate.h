//
//  MMAppDelegate.h
//  MMSmartStreamingWithBitmovin
//
//  Created by vetri on 09/20/2020.
//  Copyright (c) 2020 vetri. All rights reserved.
//

@import UIKit;

@interface MMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
