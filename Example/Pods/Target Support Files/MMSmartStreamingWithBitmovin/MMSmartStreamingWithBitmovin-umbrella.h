#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "MMSmartStreaming.h"
#import "objc_mmsmartstreaming.h"
#import "BitmovinAssetInformation.h"
#import "BitmovinPlayerIntegrationWrapper.h"
#import "MMRegistrationInformation.h"

FOUNDATION_EXPORT double MMSmartStreamingWithBitmovinVersionNumber;
FOUNDATION_EXPORT const unsigned char MMSmartStreamingWithBitmovinVersionString[];

