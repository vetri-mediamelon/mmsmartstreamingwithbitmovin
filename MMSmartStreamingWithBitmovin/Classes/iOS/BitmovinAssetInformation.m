//
//  BitmovinAssetInformation.m
//  BasicPlaybackObjectiveC
//
//  Created by MacAir 1 on 20/09/20.
//  Copyright © 2020 Bitmovin. All rights reserved.
//

#import "BitmovinAssetInformation.h"

@implementation BitmovinAssetInformation
@synthesize assetURL, assetID, assetName, videoId, qbrMode, metafileURL, customKVPs;

- (instancetype)init {
    if (self == [super init]) {
        qbrMode = QBRModeDisabled;
        customKVPs = [[NSDictionary alloc] init];
    }
    return self;
}

-(BitmovinAssetInformation *)initWithAssetURL:(NSString *) aURL assetID:(NSString *) aId assetName:(NSString *) aName videoId:(NSString *) vId {
    if (aURL != NULL) {
        assetURL = aURL;
    }
    if (aId != NULL) {
        assetID = aId;
    }
    if (aName != NULL) {
        assetName = aName;
    }
    if (vId != NULL) {
        videoId = vId;
    }
    return self;
}

-(void)addCustomKVPWithKey:(NSString *)key andvalue:(NSString *)value {
    [customKVPs setValue:value forKey:key];
}

-(void)setQbrMode:(MMQBRMode)mode withMetaURL:(NSURL *)metaURL {
    qbrMode = mode;
    if (metaURL != NULL) {
        metafileURL = metaURL;
    }
}

@end
