//
//  BitmovinPlayerIntegrationWrapper.m
//  BasicPlaybackObjectiveC
//
//  Created by MacAir 1 on 20/09/20.
//  Copyright © 2020 Bitmovin. All rights reserved.
//

#import "BitmovinPlayerIntegrationWrapper.h"
#import <BitmovinPlayer/BitmovinPlayer.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

const double TIME_INCREMENT = 2.0;

@interface BitmovinPlayerIntegrationWrapper()<MMSmartStreamingObserver, BMPPlayerListener>

#pragma mark OBJECTS
@property(nonatomic, strong)BitmovinAssetInformation * assetInformation;
@property(nonatomic, weak)BMPBitmovinPlayer * player;
@property(nonatomic)BOOL enableLogging;
@property(nonatomic, strong)NSTimer * timer;
@property(nonatomic)double seekPosition;
@property(nonatomic)double lastSentSeekPosition;
@property(nonatomic)NSTimeInterval presentationDuration;
@property(nonatomic)BOOL presentationInfoSet;
@property(nonatomic)BOOL appNotificationObsRegistered;
@property(nonatomic)MMCurrentPlayerState currentState;

@end

@implementation BitmovinPlayerIntegrationWrapper
@synthesize assetInformation, player;

+(id)shared {
    static BitmovinPlayerIntegrationWrapper *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc] init];
    });
    return shared;
}

-(instancetype)init {
    if (self = [super init]) {
        self.mmSmartStreaming = [MMSmartStreaming getInstance];
        self.currentState = MMCurrentPlayerStateIDLE;
    }
    return self;
}

+(NSString *)getVersion {
    return [NSString stringWithFormat:@"0.0.1/%@", [MMSmartStreaming getVersion]];
}

+(void)disableManifestsFetch:(BOOL)disable {
    return [MMSmartStreaming disableManifestsFetch:disable];
}

+(void)setPlayerRegistrationInformation:(MMRegistrationInformation *)regInfo andPlayer:(BMPBitmovinPlayer *)aPlayer {
    if (regInfo != NULL) {
        [BitmovinPlayerIntegrationWrapper logDebugStatement:@"=============setPlayerInformation - pInfo============="];
        [BitmovinPlayerIntegrationWrapper registerMMSmartStreamingWithPlayerName:regInfo.playerName customerID:regInfo.customerID component:regInfo.component subsciberID:regInfo.subscriberID domainName:regInfo.domainName subscriberType:regInfo.subscriberType andSubscriberTag:regInfo.subscriberTag];
        [BitmovinPlayerIntegrationWrapper reportPlayerInfoWithBrandName:regInfo.playerBrand modelName:regInfo.playerModel andVersion:regInfo.playerVersion];
    }
    
    BitmovinPlayerIntegrationWrapper * shared = [BitmovinPlayerIntegrationWrapper shared];
    if ([shared player] != NULL) {
        [shared cleanupSession:[shared player]];
    }
    if(aPlayer != NULL) {
        [shared createSession:aPlayer];
    }
}

+(void)initializeAssetForPlayerWithAssetInformation:(BitmovinAssetInformation *)aInfo registrationInformation:(MMRegistrationInformation *)regInfo andPlayer:(BMPBitmovinPlayer *)aPlayer {
    @synchronized (self) {
        [BitmovinPlayerIntegrationWrapper logDebugStatement:[NSString stringWithFormat:@"================initializeAssetForPlayer %@=============", aInfo.assetURL]];
        [BitmovinPlayerIntegrationWrapper setPlayerRegistrationInformation:regInfo andPlayer:aPlayer];
        [BitmovinPlayerIntegrationWrapper changeAssetForPlayerWithAssetInformation:aInfo andPlayer:aPlayer];
    }
}

+(void)changeAssetForPlayerWithAssetInformation:(BitmovinAssetInformation *)aInfo andPlayer:(BMPBitmovinPlayer *)aPlayer {
    [BitmovinPlayerIntegrationWrapper logDebugStatement:[NSString stringWithFormat:@"================changeAssetForPlayer %@=============", aInfo.assetURL]];
    BitmovinPlayerIntegrationWrapper * shared = [BitmovinPlayerIntegrationWrapper shared];
    shared.assetInformation = aInfo;
    [shared cleanupCurrentItem];
    
    if (aPlayer != NULL) {
        if ([shared player] != NULL) {
            if ([shared player] != aPlayer) {
                [shared cleanupSession:[shared player]];
                shared.player = NULL;
                [shared createSession:aPlayer];
            }
        }
        [shared initSessionWithPlayer:aPlayer andDeep:YES];
    }
}


+(void)registerMMSmartStreamingWithPlayerName:(NSString *)playerName customerID:(NSString *)custID component:(NSString *)component subsciberID:(NSString *)subsciberID domainName:(NSString *)domainName subscriberType:(NSString *)subscriberType andSubscriberTag:(NSString *)subscriberTag {
    [MMSmartStreaming registerMMSmartStreamingForPlayerWithName:playerName forCustID:custID component:component subsID:subsciberID domainName:domainName andSubscriberType:subscriberType withTag:subscriberTag];
    CTTelephonyNetworkInfo * phoneInfo = [CTTelephonyNetworkInfo new];
    NSString * operatorName = @"";
    NSString * carrierName = @"";
    if (phoneInfo.subscriberCellularProvider.carrierName != NULL) {
        carrierName = phoneInfo.subscriberCellularProvider.carrierName;
    }
    NSString * brand = @"Apple";
    NSString * model = [[UIDevice currentDevice] model];
    NSString * osName = @"iOS";
    NSString * osVersion = [[UIDevice currentDevice] systemVersion];
    int screenWidth = (int)[[UIScreen mainScreen] bounds].size.width;
    int screenHeight = (int)[[UIScreen mainScreen] bounds].size.height;
    
    [MMSmartStreaming reportDeviceInfoWithBrandName:brand deviceModel:model osName:osName osVersion:osVersion telOperator:operatorName screenWidth:screenWidth screenHeight:screenHeight andType:model];
}

+(void)reportPlayerInfoWithBrandName:(NSString *)brand modelName:(NSString *)model andVersion:(NSString *)version {
    [MMSmartStreaming reportPlayerInfoWithBrandName:brand model:model andVersion:version];
}

+(void)logDebugStatement:(NSString *)logStatement {
    if ([[BitmovinPlayerIntegrationWrapper shared] enableLogging]) {
        NSLog(@"%@", logStatement);
    }
}

+(void)cleanup {
    [BitmovinPlayerIntegrationWrapper logDebugStatement:@"================cleanUp============="];
    BitmovinPlayerIntegrationWrapper * shared = [BitmovinPlayerIntegrationWrapper shared];
    [shared cleanupCurrentItem];
    [shared cleanupSession:[shared player]];
}

+(void)updateSubscriberWith:(NSString *)subscriberId subscriberType:(NSString *)subscriberType andSubscriberMetadata:(NSString *)subscriberMetadata {
    [MMSmartStreaming updateSubscriberWithID:subscriberId andType:subscriberType withTag:subscriberMetadata];
}

+(void)reportMetricValue:(MMOverridableMetric)metricToOverride andValue:(NSString *)value {
    switch (metricToOverride) {
        case ServerAddress:
            [[[BitmovinPlayerIntegrationWrapper shared] mmSmartStreaming] reportMetricValueForMetric:metricToOverride value:value];
            break;
            
        default:
            NSLog(@"Only CDN metric is overridable as of now ...");
            break;
    }
}

-(void)createSession:(BMPBitmovinPlayer *)player {
    [BitmovinPlayerIntegrationWrapper logDebugStatement:@"*** createSession"];
    self.player = player;
    [self.player addPlayerListener:self];
    self.timer = NULL;
}

-(void)cleanupSession:(BMPBitmovinPlayer *)player {
    @synchronized (self) {
        if (player != NULL) {
            [player removePlayerListener:self];
        }
        self.player = NULL;
        [BitmovinPlayerIntegrationWrapper logDebugStatement:@"removeSession ***"];
    }
}

-(void)cleanupCurrentItem {
    if ([[BitmovinPlayerIntegrationWrapper shared] player] == NULL) {
        return;
    }
    
}

-(void)resetSession {
    @synchronized (self) {
        if ([[BitmovinPlayerIntegrationWrapper shared] timer] != NULL) {
            [[[BitmovinPlayerIntegrationWrapper shared] timer] invalidate];
            [[BitmovinPlayerIntegrationWrapper shared] reportStoppedState];
        }
    }
}

/**
* Application may report the custom metadata associated with the content using this API.
* Application can set it as a part of MMAVAssetInformation before the start of playback, and
* can use this API to set metadata during the course of the playback.
*/
-(void)reportCustomMetadata:(NSString *)key andValue:(NSString *)value {
    [self.mmSmartStreaming reportCustomMetadataWithKey:key andValue:value];
}

/**
* Used for debugging purposes, to enable the log trace
*/
-(void)enableLogTrace:(BOOL)logStTrace {
    BitmovinPlayerIntegrationWrapper * shared = [BitmovinPlayerIntegrationWrapper shared];
    shared.enableLogging = logStTrace;
    [self.mmSmartStreaming enableLogTrace:logStTrace];
}

/**
* If application wants to send application specific error information to SDK, the application can use this API.
* Note - SDK internally takes care of notifying the error messages provided to it by AVFoundation framwork
*/
-(void)reportErrorWith:(NSString *)error andPlaybackPosMilliSec:(int)playbackPosMilliSec {
    [self.mmSmartStreaming reportError:error atPosition:playbackPosMilliSec];
}


-(void)initSessionWithPlayer:(BMPBitmovinPlayer *)aPlayer andDeep:(BOOL)deep {
    [BitmovinPlayerIntegrationWrapper logDebugStatement:@"*** initSession"];
    
    if (deep) {
        [self reset];
    }
    
    if (self.assetInformation == NULL) {
        [BitmovinPlayerIntegrationWrapper logDebugStatement:@"!!! Error - assetInfo not set !!!"];
        return;
    }
    
    if (deep) {
        [self initializeSessionWithMode:self.assetInformation.qbrMode manifest:self.assetInformation.assetURL metaURL:self.assetInformation.metafileURL assetID:self.assetInformation.assetID assetName:self.assetInformation.assetName videoID:self.assetInformation.videoId];
        [self.assetInformation.customKVPs enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            [[BitmovinPlayerIntegrationWrapper shared] reportCustomMetadataWithKey:key andValue:obj];
        }];
        [self reportUserInitiatedPlayback];
    } else {
        [self.assetInformation.customKVPs enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            [[BitmovinPlayerIntegrationWrapper shared] reportCustomMetadataWithKey:key andValue:obj];
        }];
    }
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:TIME_INCREMENT target:self selector:@selector(timeoutOccurred) userInfo:nil repeats:YES];
    
    [BitmovinPlayerIntegrationWrapper logDebugStatement:[NSString stringWithFormat:@"Initializing for %@", self.assetInformation.assetURL]];
}

-(void)initializeSessionWithMode:(MMQBRMode)mode manifest:(NSString *)manifest metaURL:(NSString *)metaURL assetID:(NSString *)assetID assetName:(NSString *)assetName videoID:(NSString *)videoID {
    
    //NETWORK REACHABILITY
    [self.mmSmartStreaming reportNetworkType:MM_Wifi];
    
    [self.mmSmartStreaming initializeSessionWithMode:mode forManifest:manifest metaURL:metaURL assetID:assetID assetName:assetName videoID:videoID forObserver:self];
}

-(void)sessionInitializationCompletedWithStatus:(MMSmartStreamingInitializationStatus)status andDescription:(NSString *)description forCmdWithId:(NSInteger)cmdId {
    [BitmovinPlayerIntegrationWrapper logDebugStatement:[NSString stringWithFormat:@"sessionInitializationCompleted - status %ld description %@", (long)status, description]];
}

-(void)reset {
    self.presentationInfoSet = NO;
    self.currentState = MMCurrentPlayerStateIDLE;
    
    if (!self.appNotificationObsRegistered) {
        self.appNotificationObsRegistered = YES;
        
        [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillResignActiveNotification object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
            //ReachabilityManager.shared.stopMonitoring()
        }];
        
        [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidBecomeActiveNotification object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
            //ReachabilityManager.shared.startMonitoring()
        }];
    }
}

-(void)reportStoppedState {
    if (self.currentState != MMCurrentPlayerStateIDLE && self.currentState != MMCurrentPlayerStateSTOPPED) {
        if (self.player != NULL) {
            NSTimeInterval time = self.player.currentTime;
            if (time > 0) {
                [self.mmSmartStreaming reportPlaybackPosition:(int)(time * 1000)];
            }
            [self.mmSmartStreaming reportPlayerState:STOPPED];
            self.currentState = MMCurrentPlayerStateSTOPPED;
        }
    }
}

-(void)timeoutOccurred {
    
}

-(void)reportUserInitiatedPlayback {
    [self.mmSmartStreaming reportUserInitiatedPlayback];
}

-(void)reportABRSwitchWith:(NSInteger)prevBitrate andNewBitrate:(NSInteger)newBitrate {
    [self.mmSmartStreaming reportABRSwitchFromBitrate:prevBitrate toBitrate:newBitrate];
}

-(void)reportBufferingStarted {
    [self.mmSmartStreaming reportBufferingStarted];
}

-(void)reportBufferingCompleted {
    [self.mmSmartStreaming reportBufferingCompleted];
}

-(void)reportPlayerSeekCompleted:(int)seekEndPos {
    [self.mmSmartStreaming reportPlayerSeekCompleted:seekEndPos];
}

-(void)reportPresentationSizeWithWidth:(NSInteger)width andHeight:(NSInteger)height {
    [self.mmSmartStreaming reportPresentationSizeWithWidth:width andHeight:height];
}

-(void)reportDownloadRate:(NSInteger)downloadRate {
    [self.mmSmartStreaming reportDownloadRate:downloadRate];
}

-(void)setPresentationInformationForContent {
    if (self.presentationDuration < 0) {
        return;
    }
    
    if(!self.presentationInfoSet) {
        MMPresentationInfo * presentationInfo = [MMPresentationInfo new];
        if (self.player.isLive) {
            presentationInfo.isLive = YES;
            presentationInfo.duration = (int)-1;
        } else {
            presentationInfo.isLive = NO;
            presentationInfo.duration = (int)(self.player.duration * 1000);
        }
        [self.mmSmartStreaming setPresentationInformation:presentationInfo];
        self.presentationInfoSet = YES;
    }
}

-(int)getPlaybackPosition {
    if (self.player == NULL) {
        return 0;
    }
    int currentPlaybackPosition = (int)(self.player.currentTime * 1000);
    if (currentPlaybackPosition > 0) {
        return currentPlaybackPosition * 1000;
    } else {
        return 0;
    }
}

-(void)processDuration:(NSTimeInterval)changedTime {
    if (self.player == NULL) {
        return;
    }
    self.presentationDuration = changedTime;
    [self setPresentationInformationForContent];
    BMPVideoQuality * videoQuality = self.player.videoQuality;
    if (videoQuality != NULL) {
        [self reportPresentationSizeWithWidth:videoQuality.width andHeight:videoQuality.height];
        [self reportDownloadRate:videoQuality.bitrate];
    }
}

-(void)processDurationFromPlayerItem {
    if (self.player == NULL) {
        return;
    }
    self.presentationDuration = self.player.duration;
    [self setPresentationInformationForContent];
}

#pragma mark BMPPlayerListers

-(void)onSourceLoaded:(BMPSourceLoadedEvent *)event {
    
}

- (void)onReady:(BMPReadyEvent *)event {
    [self processDurationFromPlayerItem];
}

- (void)onPlay:(BMPPlayEvent *)event {
    
}

-(void)onPlaying:(BMPPlayingEvent *)event {
    [self.mmSmartStreaming reportPlayerState:PLAYING];
    self.currentState = PLAYING;
}

- (void)onPaused:(BMPPausedEvent *)event {
    [self.mmSmartStreaming reportPlayerState:PAUSED];
    self.currentState = PAUSED;
}

- (void)onTimeChanged:(BMPTimeChangedEvent *)event {
    //[self processDuration:event.currentTime];
}

- (void)onDurationChanged:(BMPDurationChangedEvent *)event {
    [self processDuration:event.duration];
}

- (void)onSeek:(BMPSeekEvent *)event {
    self.seekPosition = (double)event.position;
}

- (void)onSeeked:(BMPSeekedEvent *)event {
    if (self.seekPosition != self.lastSentSeekPosition) {
        self.lastSentSeekPosition = self.seekPosition;
        [self reportPlayerSeekCompleted:(int)(self.seekPosition * 1000)];
    }
}

- (void)onStallStarted:(BMPStallStartedEvent *)event {
    [self reportBufferingStarted];
}

-(void)onStallEnded:(BMPStallEndedEvent *)event {
    [self reportBufferingCompleted];
}

- (void)onVideoDownloadQualityChanged:(BMPVideoDownloadQualityChangedEvent *)event {
    NSInteger oldBitrate = 0;
    NSInteger newBitrate = 0;
    if (event.videoQualityOld != NULL) {
        oldBitrate = event.videoQualityOld.bitrate;
    }
    if (event.videoQualityNew != NULL) {
        newBitrate = event.videoQualityNew.bitrate;
    }
    [self reportABRSwitchWith:oldBitrate andNewBitrate:newBitrate];
}

- (void)onDownloadFinished:(BMPDownloadFinishedEvent *)event {
    
}

- (void)onPlaybackFinished:(BMPPlaybackFinishedEvent *)event {
    [self cleanupCurrentItem];
}

- (void)onError:(BMPErrorEvent *)event {
    [self reportErrorWith:event.message andPlaybackPosMilliSec:[self getPlaybackPosition]];
}

- (void)onWarning:(BMPWarningEvent *)event {
    //[self reportErrorWith:event.message andPlaybackPosMilliSec:[self getPlaybackPosition]];
}

-(void)onDestroy:(BMPDestroyEvent *)event {
    [self reportStoppedState];
}
             
             
@end
