//
//  BitmovinAssetInformation.h
//  BasicPlaybackObjectiveC
//
//  Created by MacAir 1 on 20/09/20.
//  Copyright © 2020 Bitmovin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "objc_mmsmartstreaming.h"

NS_ASSUME_NONNULL_BEGIN

@interface BitmovinAssetInformation : NSObject

#pragma mark PUBLIC OBJECTS
@property (nonatomic, strong) NSString * assetURL; //URL of the Asset
@property (nonatomic, weak) NSString * assetID; //optional identifier of the asset
@property (nonatomic, weak) NSString * assetName; //optional name of the asset
@property (nonatomic, weak) NSString * videoId; //optional identifier of the asset group (or) sub asset
@property (nonatomic) MMQBRMode qbrMode; //Needed only when QBR is to be integrated
@property (nonatomic, weak) NSURL * metafileURL; //Needed only when QBR is to be integrated
@property (nonatomic, strong) NSDictionary * customKVPs; //Custom Metadata of the asset

#pragma mark PUBLIC METHODS

/*
* Creates an object to hold information identifying the asset that is to be played back on the BitMovinPlayer
* User must specify URL of the asset.
* User may optionally specify the identifier identifying the asset, its name and collection to which this asset belongs
*/
-(BitmovinAssetInformation *)initWithAssetURL:(NSString *)aURL assetID:(NSString *)aId assetName:(NSString *)aName videoId:(NSString *)vId;

/*
* Lets user specify the custom metadata to be associated with the asset, for example - Genre, DRM etc
*
* Call to this API is optional
*/
-(void)addCustomKVPWithKey:(NSString *)key andvalue:(NSString *)value;

/*
* Sets the mode to be used for QBR and the meta file url from where content metadata can be had.
* Meta file URL is to be provided only if metadata cant be had on the default location.
*
* Please note that call to this method is needed only if QBR is integrated to the player.
*/
-(void)setQbrMode:(MMQBRMode)mode withMetaURL:(NSURL *)metaURL;

@end

NS_ASSUME_NONNULL_END
