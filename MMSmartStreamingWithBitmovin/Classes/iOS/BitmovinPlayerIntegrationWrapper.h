//
//  BitmovinPlayerIntegrationWrapper.h
//  BasicPlaybackObjectiveC
//
//  Created by MacAir 1 on 20/09/20.
//  Copyright © 2020 Bitmovin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BitmovinPlayer/BitmovinPlayer.h>

#import "MMRegistrationInformation.h"
#import "BitmovinAssetInformation.h"

NS_ASSUME_NONNULL_BEGIN

@interface BitmovinPlayerIntegrationWrapper : NSObject

#pragma mark PUBLIC OBJECTS
@property(nonatomic, strong) MMSmartStreaming * mmSmartStreaming;

typedef NS_ENUM(NSInteger, MMCurrentPlayerState){
    MMCurrentPlayerStateIDLE = 1,
    MMCurrentPlayerStatePLAYING,
    MMCurrentPlayerStatePAUSED,
    MMCurrentPlayerStateSTOPPED,
    MMCurrentPlayerStateERROR
};

#pragma mark PUBLIC METHODS

/**
* Singleton instance of the adaptor
*/
+(id)shared;

/**
* Gets the version of the SDK
*/

+(NSString *)getVersion;

/**
* If for some reasons, accessing the content manifest by SDK interferes with the playback. Then user can disable the manifest fetch by the SDK.
* For example - If there is some token scheme in content URL, that makes content to be accessed only once, then user of SDK may will like to call this API.
* So that player can fetch the manifest
*/
+(void)disableManifestsFetch:(BOOL)disable;

/**
* Allows user of SDK to provide information on Customer, Subscriber and Player to the SDK
* Please note that it is sufficient to call this API only once for the lifetime of the application, and all playback sessions will reuse this information.
*
* Note - User may opt to call initializeAssetForPlayer instead of calling this API, and provide the registration information in its param every time as they provide the asset info. This might help ease the integration.
*
* This API doesnt involve any network IO, and is very light weight. So calling it multiple times is not expensive
*/

+(void)setPlayerRegistrationInformation:(MMRegistrationInformation *)regInfo andPlayer:(BMPBitmovinPlayer *)aPlayer;

/**
* Application may create the player with the AVAsset for every session they do the playback
* User of API must provide the asset Information
*/

+(void)initializeAssetForPlayerWithAssetInformation:(BitmovinAssetInformation *)aInfo registrationInformation:(MMRegistrationInformation *)regInfo andPlayer:(BMPBitmovinPlayer *)aPlayer;

/**
* Whenever the asset with the player is changed, user of the API may call this API
* Please note either changeAssetForPlayer or initializeAssetForPlayer should be called
*/

+(void)changeAssetForPlayerWithAssetInformation:(BitmovinAssetInformation *)aInfo andPlayer:(BMPBitmovinPlayer *)aPlayer;

/**
* Once the player is done with the playback session, then application should call this API to clean up observors set with the player and the player's current item
*/

+(void)cleanup;

/**
* Application may update the subscriber information once it is set via MMRegistrationInformation
*/

+(void)updateSubscriberWith:(NSString *)subscriberId subscriberType:(NSString *)subscriberType andSubscriberMetadata:(NSString *)subscriberMetadata;

+(void)reportMetricValue:(MMOverridableMetric)metricToOverride andValue:(NSString *)value;

@end

NS_ASSUME_NONNULL_END
