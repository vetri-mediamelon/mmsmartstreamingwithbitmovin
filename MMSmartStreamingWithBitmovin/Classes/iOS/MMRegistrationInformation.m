//
//  MMRegistrationInformation.m
//  BasicPlaybackObjectiveC
//
//  Created by MacAir 1 on 20/09/20.
//  Copyright © 2020 Bitmovin. All rights reserved.
//

#import "MMRegistrationInformation.h"

@implementation MMRegistrationInformation
@synthesize customerID, component, playerName, domainName, subscriberID, subscriberType, subscriberTag, playerBrand, playerModel, playerVersion;

- (instancetype)init {
    if (self == [super init]) {
        
    }
    return self;
}

-(MMRegistrationInformation *)initWithCustomerID:(NSString *)cID andPlayerName:(NSString *)pName {
    component = @"IOSSDK";
    if (cID != NULL) {
        customerID = cID;
    }
    if (pName != NULL) {
        playerName = pName;
    }
    return self;
}

-(void)setDomain:(NSString *)dName {
    if (dName != NULL) {
        domainName = dName;
    }
}

-(void)setSubscriberInformationWithsubscriberID:(NSString *)subsID subscriberType:(NSString *)subsType andSubscriberTag:(NSString *)subsTag {
    if (subsID != NULL) {
        subscriberID = subsID;
    }
    if (subsType != NULL) {
        subscriberType = subsType;
    }
    if (subsTag != NULL) {
        subscriberTag = subsTag;
    }
}

-(void)setPlayerInformationWithBrand:(NSString *)brand model:(NSString *)model andVersion:(NSString *)version {
    if (brand != NULL) {
        playerBrand = brand;
    }
    if (model != NULL) {
        playerModel = model;
    }
    if (version != NULL) {
        playerVersion = version;
    }
}

@end
