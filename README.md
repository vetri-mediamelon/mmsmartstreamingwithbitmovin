# MMSmartStreamingWithBitmovin

[![CI Status](https://img.shields.io/travis/vetri/MMSmartStreamingWithBitmovin.svg?style=flat)](https://travis-ci.org/vetri/MMSmartStreamingWithBitmovin)
[![Version](https://img.shields.io/cocoapods/v/MMSmartStreamingWithBitmovin.svg?style=flat)](https://cocoapods.org/pods/MMSmartStreamingWithBitmovin)
[![License](https://img.shields.io/cocoapods/l/MMSmartStreamingWithBitmovin.svg?style=flat)](https://cocoapods.org/pods/MMSmartStreamingWithBitmovin)
[![Platform](https://img.shields.io/cocoapods/p/MMSmartStreamingWithBitmovin.svg?style=flat)](https://cocoapods.org/pods/MMSmartStreamingWithBitmovin)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MMSmartStreamingWithBitmovin is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MMSmartStreamingWithBitmovin'
```

## Author

vetri, vetri@mediamelon.com

## License

MMSmartStreamingWithBitmovin is available under the MIT license. See the LICENSE file for more info.
