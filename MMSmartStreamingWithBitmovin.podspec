#
# Be sure to run `pod lib lint MMSmartStreamingWithBitmovin.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'MMSmartStreamingWithBitmovin'
  s.version          = '0.1.1'
  s.summary          = 'The MediaMelon Player SDK Provides SmartSight Analytics and QBR SmartStreaming.'
  s.description      = 'The MediaMelon Player SDK adds SmartSight Analytics and QBR SmartStreaming capability to any media player and is available for all ABR media players.'
  s.homepage         = 'https://vetri-mediamelon@bitbucket.org/vetri-mediamelon/mmsmartstreamingwithbitmovin'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'vetri' => 'vetri@mediamelon.com' }
  s.source           = { :git => 'https://vetri-mediamelon@bitbucket.org/vetri-mediamelon/mmsmartstreamingwithbitmovin.git', :tag => '0.1.1' }
  s.ios.deployment_target = '10.0'
  s.source_files = 'MMSmartStreamingWithBitmovin/Classes/Common/**/*.{h,swift}'
  s.ios.source_files = 'MMSmartStreamingWithBitmovin/Classes/iOS/*.{h,m}'
  s.frameworks = 'UIKit', 'AVFoundation', 'CoreGraphics', 'SystemConfiguration'
  s.ios.vendored_libraries = 'MMSmartStreamingWithBitmovin/Classes/iOS/libmmsmartstreamer.a'
  s.libraries = 'stdc++'
  s.public_header_files = 'MMSmartStreamingWithBitmovin/Classes/Common/MMSmartStreaming/**/*.h' , 'MMSmartStreamingWithBitmovin/Classes/iOS/*.h'
  s.vendored_frameworks = 'MMSmartStreamingWithBitmovin/Frameworks/BitmovinPlayer.framework'
  s.static_framework = true
end
